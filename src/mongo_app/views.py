import random

from django.http import HttpResponse

from mongo_app.models import Blog, Entry


def create_in_mongo(request):
    saved = Entry(
        blog=[
            Blog(
                name="".join([random.choice("abcde") for _ in range(3)]),
                tagline="".join([random.choice("abcde") for _ in range(3)]),
            )
        ],
        headline="some text",
    ).save()

    return HttpResponse(f"Done: {saved}")


def all_entries(request):
    blogs_timestamps = list(Entry.objects.all().values_list("timestamp"))
    print(blogs_timestamps)

    return HttpResponse(
        "| ".join(
            [timestamp.strftime("%m/%d/%Y, %H:%M:%S") for timestamp in blogs_timestamps]
        )
    )
