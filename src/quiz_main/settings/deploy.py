import os
from quiz_main.settings.base import *  # noqa:

DEBUG = False

ALLOWED_HOSTS = ["localhost"]

MEDIA_ROOT = os.path.join(BASE_DIR, "media/")  # noqa:
STATIC_ROOT = os.path.join(BASE_DIR, "static/")  # noqa:
